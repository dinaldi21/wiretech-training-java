package com.wtid.Tugas4.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wtid.Tugas4.domain.*;

public class OrderRepositoryMysql implements OrderRepositoryInterface {
	
	
	public List<Order> findAll() throws IOException{
		Set<String> orderIdSet = new HashSet<String>();
		List<OrderItem> orderItemList = new ArrayList<OrderItem>();
		List<Order> listOrder = new ArrayList<Order>();
		File file = new File("teksmysql.txt");
		FileReader reader = new FileReader(file);
		BufferedReader buffRd = new BufferedReader(reader);
		String line = null;
		while ((line = buffRd.readLine())!= null) {
			String[] data = line.split(";");
			String orderId = data[0].trim();
			String code = data[1].trim();
			String name = data[2].trim();
			String type = data[3].trim();
			Double price = Double.parseDouble(data[4].trim());
			int quantity = Integer.parseInt(data[5].trim());
			OrderItem item = new OrderItem(orderId, code, name, type, price);
			item.setQuantity(quantity);
			orderItemList.add(item);
			orderIdSet.add(orderId);
		}
		buffRd.close();
		
		for (String orderId : orderIdSet) {
			Order order = new Order(Integer.parseInt(orderId));
			for (OrderItem item : orderItemList) {
				if(orderId.equals(item.getOrderId())) {
					order.addItem(item);
				}
			}
			listOrder.add(order);
		}
		
		return listOrder;
	}
	
	public Order findById (int orderId) throws IOException {
		List<Order> orders = findAll();
		for (Order order : orders) {
			if (orderId == order.getId()) {
				return order;
			}
		}
		return null;
	}
	
}
