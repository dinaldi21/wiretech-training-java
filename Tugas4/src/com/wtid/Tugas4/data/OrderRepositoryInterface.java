package com.wtid.Tugas4.data;

import java.io.IOException;
import java.util.List;

import com.wtid.Tugas4.domain.Order;

public interface OrderRepositoryInterface {
	List<Order> findAll() throws IOException;
	Order findById (int orderId) throws IOException;
}
