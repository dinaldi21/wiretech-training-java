package com.wtid.Tugas4.data;

public class OrderRepositorySet {
	public static String set = "Mysql";
	
	public static OrderRepositoryInterface getOrderRepositoryInterface() {
		if ("Mysql".equals(set)) {
			return new OrderRepositoryMysql();
		} else {
			return new OrderRepositoryFile();
		}
	}
}
