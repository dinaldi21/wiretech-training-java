package com.wtid.Tugas4.domain;

public class OrderItem {
	String code, name, type, orderId;
	double price;
	int quantity;
	
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public double getPrice() {
		return price;
	}
	public int getQuantity() {
		return quantity;
	}
	
	public String getOrderId() {
		return orderId;
	}
	

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public OrderItem(String orderId, String code, String name, String type, double price) {
		this.orderId = orderId;
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
		this.quantity = 1;
	}

	
	public double totalPrice() {
		return price*quantity;
	}
}
