package com.wtid.Tugas4.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Order {
	private int id;
	private Date createdAt;
	List<OrderItem> orderitem = new ArrayList<OrderItem>();	
		
	public int getId() {
		return id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}	
	
	public Order(int id) {
		this.id = id;
		createdAt = new Date();
	}
	
	
	public List<OrderItem> getOrderitem() {
		return orderitem;
	}
	
	public void addItem(OrderItem orderitem) {
		this.orderitem.add(orderitem);
	}
	
	public double totalPrice() {
		double total = 0;

		for (OrderItem orderitem : this.orderitem) {
			total = total + orderitem.totalPrice();
			}
		return total;
	}

}