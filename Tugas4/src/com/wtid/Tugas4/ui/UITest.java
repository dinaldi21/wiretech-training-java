package com.wtid.Tugas4.ui;

import java.io.IOException;
import java.util.List;

import com.wtid.Tugas4.data.OrderRepositoryFile;
import com.wtid.Tugas4.data.OrderRepositoryInterface;
import com.wtid.Tugas4.data.OrderRepositoryMysql;
import com.wtid.Tugas4.data.OrderRepositorySet;
import com.wtid.Tugas4.domain.Order;
import com.wtid.Tugas4.domain.OrderItem;

public class UITest {

	public static void main(String[] args) {
		OrderRepositoryInterface repo = OrderRepositorySet.getOrderRepositoryInterface();
		List<Order> orders;
		try {
			orders = repo.findAll();
			for (Order order : orders) {
				System.out.println("Order ID : "+ order.getId());
				for (OrderItem item : order.getOrderitem()) {
					System.out.println(item.getCode()+" "+item.getName()+" "+item.getPrice());
				}
				System.out.println(order.totalPrice());
				System.out.println("");
				System.out.println("");
			
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//			Order order = repo.findById(1);
//			System.out.println("Order ID : "+ order.getId());
//			for (OrderItem item : order.getOrderitem()) {
//				System.out.println(item.getCode()+" "+item.getName()+" "+item.getPrice());
//			}
//			System.out.println(order.totalPrice());
//
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

}
